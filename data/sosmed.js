import { IoLogoLinkedin, IoLogoGithub } from "react-icons/io5";

export const SOSMED_DATA = [
  {
    id: 1,
    link: "https://github.com/iqbal387",
    icon: <IoLogoGithub />,
    style: { color: "inherit" },
  },
  {
    id: 2,
    link: "https://www.linkedin.com/in/iqbalhasan387",
    icon: <IoLogoLinkedin />,
    style: { color: "#2867B2" },
  },
];
