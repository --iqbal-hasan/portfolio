import Image from "next/image";
import {
  IoLogoReact,
  IoLogoHtml5,
  IoLogoCss3,
  IoLogoVue,
  IoLogoNodejs,
  IoLogoJavascript,
} from "react-icons/io5";
import {
  SiNextDotJs,
  SiNuxtDotJs,
  SiGraphql,
  SiTypescript,
  SiFlutter,
} from "react-icons/si";

export const EXPERTISE_DATA = [
  {
    label: "Next JS",
    icon: <SiNextDotJs />,
    style: { color: "inherit" },
  },
  {
    label: "React JS",
    icon: <IoLogoReact />,
    style: { color: "#61DBFB" },
  },
  {
    label: "React Native",
    icon: <IoLogoReact />,
    style: { color: "#007ACC" },
  },
  {
    label: "Flutter",
    icon: <SiFlutter />,
    style: { color: "#042B59" },
  },
  {
    label: "Typescript",
    icon: <SiTypescript />,
    style: { color: "#007ACC" },
  },
  {
    label: "Vite",
    icon: <Image src="/vite.svg" alt="Vite" width={48} height={40} />,
    style: { color: "#F0DB4F" },
  },
  {
    label: "Bun",
    icon: <Image src="/bun.svg" alt="Bun" width={48} height={40} />,
    style: { color: "#F0DB4F" },
  },
  {
    label: "Vue JS",
    icon: <IoLogoVue />,
    style: { color: "#41B883" },
  },
  {
    label: "Nuxt JS",
    icon: <SiNuxtDotJs />,
    style: { color: "#41b883" },
  },
  {
    label: "Node JS",
    icon: <IoLogoNodejs />,
    style: { color: "#3C873A" },
  },
  {
    label: "GraphQL",
    icon: <SiGraphql />,
    style: { color: "#e535ab" },
  },
];
