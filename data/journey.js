import { IoSchoolOutline, IoCodeSlashOutline } from "react-icons/io5";

export const JOURNEY_DATA = [
  {
    label: "Education",
    icon: <IoSchoolOutline />,
    data: [
      {
        label: "Informatic Engineering",
        from: "Universitas Komputer Indonesia",
        year: "Sep 2015 - Sep 2019",
      },
    ],
  },
  {
    label: "Experience",
    icon: <IoCodeSlashOutline />,
    data: [
      {
        label: "Frontend Developer - Web & Mobile",
        from: "PT. Ethis Fintek Indonesia",
        year: "Sep 2021 - now",
      },
      {
        label: "Frontend Developer - Web",
        from: "PT. Surya Teknologi Nasioanal",
        year: "Oct 2019 - Sep 2021",
      },
      {
        label: "Frontend Developer - Web & Mobile (Project)",
        from: "Ulas.id",
        year: "2020",
      },
    ],
  },
];
