import { useContext } from "react";
import { IoCalendarOutline } from "react-icons/io5";

import { MenuContext } from "@pages/_app";

import { JOURNEY_DATA } from "@data/index";

const Journey = () => {
  // context
  const { journeyRef } = useContext(MenuContext);

  return (
    <section ref={journeyRef} className="max-w-7xl w-full py-32 mx-auto">
      <h1 className="sm:text-center text-5xl font-bold mb-20">
        Awesome Journey
      </h1>

      <div className="flex flex-wrap items-start space-y-8 sm:space-y-0">
        {JOURNEY_DATA.map((journey) => (
          <div
            key={journey.label}
            className="w-full sm:w-1/2 flex flex-col space-y-8"
          >
            <h2 className="text-3xl font-bold flex items-center">
              <span className="mr-5 text-5xl">{journey.icon}</span>
              {journey.label}
            </h2>

            <ul className="flex flex-col space-y-6">
              {journey.data.map((item) => (
                <li key={item.label} className="w-full flex items-stretch">
                  <div className="w-20 flex justify-center pt-1 relative">
                    <div className="absolute t-0 w-5 h-5 rounded-full border border-primary flex justify-center items-center z-40 bg-white dark:bg-dark1">
                      <div className="w-3 h-3 rounded-full bg-primary" />
                    </div>

                    <div className="border border-primary" />
                  </div>
                  <div className="w-full">
                    <h3 className="text-xl font-bold">{item.label}</h3>

                    <h4 className="text-md italic text-gray-500 dark:text-gray-300">
                      {item.from}
                    </h4>

                    <p className="mt-3 text-sm text-primary flex items-center">
                      <IoCalendarOutline className="mr-2 text-base" />
                      {item.year}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </section>
  );
};

export default Journey;
