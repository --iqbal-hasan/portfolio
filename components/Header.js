import { useContext, useRef, useMemo } from "react";
import Image from "next/image";

import { MenuContext } from "@pages/_app";

import useWindowSize from "@hooks/useWindowSize";

import ContactDialog from "./ContactDialog";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobile from "./HeaderMobile";

const Header = ({ darkMode, setDarkMode }) => {
  // context
  const { homeRef, aboutRef, expertiseRef, journeyRef, portfolioRef } =
    useContext(MenuContext);

  // size
  const size = useWindowSize();

  // ref
  const contactDialogRef = useRef(null);

  const openContactDialog = () => {
    contactDialogRef.current.handleClickOpen();
  };

  const headers = useMemo(
    () => [
      {
        label: "Home",
        ref: homeRef,
      },
      {
        label: "About Me",
        ref: aboutRef,
      },
      {
        label: "Expertise",
        ref: expertiseRef,
      },
      {
        label: "Journey",
        ref: journeyRef,
      },
      {
        label: "Portfolio",
        ref: portfolioRef,
      },
    ],
    []
  );

  const headerProps = {
    darkMode,
    setDarkMode,
    headers,
    openContactDialog,
  };

  return (
    <>
      <header className="w-full border-b border-gray-200 dark:border-gray-600 bg-white dark:bg-dark1 fixed top-0 left-0 z-50 pl-4 pr-2">
        <nav className="max-w-7xl w-full mx-auto flex justify-end items-center">
          {size.width >= 992 && <HeaderDesktop {...headerProps} />}

          {size.width < 992 && <HeaderMobile {...headerProps} />}
        </nav>
      </header>

      <ContactDialog ref={contactDialogRef} />
    </>
  );
};

export default Header;
