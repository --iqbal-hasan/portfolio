import { useContext, useRef } from "react";
import Image from "next/image";
import { IoCodeSlashOutline, IoListOutline } from "react-icons/io5";

import useWindowSize from "@hooks/useWindowSize";

import { MenuContext } from "@pages/_app";

import Button from "./Button";
import ContactDialog from "./ContactDialog";

const Banner = () => {
  // size
  const size = useWindowSize();

  // context
  const { homeRef } = useContext(MenuContext);

  // ref
  const contactDialogRef = useRef(null);

  const openContactDialog = () => {
    contactDialogRef.current.handleClickOpen();
  };

  const handleExperienceYears = () => {
    const startDate = new Date("2019-09-01");
    const currentDate = new Date();

    const result = currentDate - startDate;

    const days = Math.round(result / (1000 * 60 * 60 * 24 * 360));

    return days;
  };

  return (
    <>
      <section ref={homeRef} className="relative">
        <div className="min-h-screen max-w-7xl w-full py-32 mx-auto flex items-center justify-between">
          <div className="max-w-lg w-full flex flex-col space-y-4 pr-0 md:pr-6">
            <h5 className="text-3xl font-bold text-secondary dark:text-white">
              Hi, I am
            </h5>

            <h1 className="text-6xl font-bold text-primary">Iqbal Hasan</h1>

            <h5 className="text-3xl font-bold text-secondary dark:text-white">
              Frontend Developer
            </h5>

            <p className="text-gray-500 dark:text-gray-300">
              Have a passion in programming, especially on Frontend using React.
              Have good logic and able to develop themselves. Able to adapt to
              the environment and have a willingness to learn new knowledge.
            </p>

            <div className="flex flex-wrap space-x-4 pt-4">
              <Button color="secondary" onClick={openContactDialog}>
                Contact Me
              </Button>
            </div>
          </div>

          <div>
            {size.width >= 600 && (
              <Image
                src="/code.svg"
                alt="web developer"
                width={650}
                height={450}
              />
            )}
          </div>
        </div>

        <div className="relative md:absolute -bottom-14 w-full flex justify-center">
          <ul className="rounded-full py-3 md:py-6 px-0 md:px-14 border-none md:border border-gray-200 dark:border-gray-600 flex space-x-4 md:space-x-8">
            <li className="flex items-center space-x-2 md:space-x-3">
              <div className="p-4 text-xl md:text-3xl rounded-full bg-primary dark:bg-transparent text-white dark:text-primary border border-primary">
                <IoCodeSlashOutline />
              </div>

              <div>
                <h5 className="text-md md:text-lg font-semibold">
                  {handleExperienceYears()}+ Years Job
                </h5>

                <h6 className="text-gray-500 dark:text-gray-300 text-sm md:text-base">
                  Experience
                </h6>
              </div>
            </li>

            <li className="border border-gray-200 dark:border-gray-600 hidden md:block" />

            <li className="flex items-center space-x-2 md:space-x-3">
              <div className="p-4 text-xl md:text-3xl rounded-full bg-primary dark:bg-transparent text-white dark:text-primary border border-primary">
                <IoListOutline />
              </div>

              <div>
                <h5 className="text-md md:text-lg font-semibold">
                  12+ Projects
                </h5>

                <h6 className="text-gray-500 dark:text-gray-300 text-sm md:text-base">
                  Completed
                </h6>
              </div>
            </li>
          </ul>
        </div>
      </section>

      <ContactDialog ref={contactDialogRef} />
    </>
  );
};

export default Banner;
