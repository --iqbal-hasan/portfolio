const Button = ({ children, color = "primary", ...props }) => (
  <button
    className={`py-2 px-5 rounded capitalize text-sm font-medium inline-block border  dark:bg-transparent dark:text-white ${
      color === "primary"
        ? "bg-primary text-white border-primary hover:bg-opacity-75"
        : "bg-secondary text-white border-secondary hover:bg-opacity-75 hover:text-dark1 dark:border-primary"
    }`}
    {...props}
  >
    {children}
  </button>
);

export default Button;
