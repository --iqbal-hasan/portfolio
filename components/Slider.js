import { useRef } from "react";
import { BsArrowLeft, BsArrowRight } from "react-icons/bs";

const Slider = ({ data = [], children }) => {
  // ref
  const sliderRef = useRef();

  const handlePrev = () => {
    const slide = sliderRef.current;
    slide.scrollLeft -= slide.offsetWidth;
    if (slide.scrollLeft <= 0) {
      slide.scrollLeft = slide.scrollWidth;
    }
  };

  const handleNext = () => {
    const slide = sliderRef.current;
    slide.scrollLeft += slide.offsetWidth;
    if (slide.scrollLeft >= slide.scrollWidth - slide.offsetWidth) {
      slide.scrollLeft = 0;
    }
  };

  return (
    <div className="relative">
      <div ref={sliderRef} className="w-full inline-flex overflow-hidden">
        {children}
      </div>

      <div
        className="absolute top-1/2 -translate-y-1/2 left-1 p-2 cursor-pointer border border-1 border-primary rounded-full bg-dark2"
        style={{
          transform: "translateY(-50%)",
        }}
        onClick={handlePrev}
      >
        <BsArrowLeft className="text-primary" />
      </div>

      <div
        className="absolute top-1/2 right-1 p-2 cursor-pointer border border-1 border-primary rounded-full bg-dark2"
        style={{
          transform: "translateY(-50%)",
        }}
        onClick={handleNext}
      >
        <BsArrowRight className="text-primary" />
      </div>
    </div>
  );
};

export default Slider;
