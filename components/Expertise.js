import { useContext } from "react";

import { MenuContext } from "@pages/_app";

import { EXPERTISE_DATA } from "@data/index";

const Expertise = () => {
  // context
  const { expertiseRef } = useContext(MenuContext);

  return (
    <section ref={expertiseRef} className="max-w-7xl w-full py-32 mx-auto">
      <h1 className="text-center text-5xl font-bold mb-20">
        My Expertise Area
      </h1>

      <div className="flex flex-wrap justify-center items-start max-w-xl w-full mx-auto">
        {EXPERTISE_DATA.map((item) => (
          <div
            key={item.label}
            className="flex flex-col items-center my-4 mx-6"
          >
            <h1 className="text-5xl mb-2" style={item.style}>
              {item.icon}
            </h1>

            <h5 className="font-semibold">{item.label}</h5>
          </div>
        ))}
      </div>
    </section>
  );
};

export default Expertise;
