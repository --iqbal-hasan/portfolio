import { IoMoonOutline, IoMoonSharp } from "react-icons/io5";

import { handleMenuClicked } from "@utils/methods";

import Button from "./Button";

const HeaderDesktop = ({
  darkMode,
  setDarkMode,
  headers,
  openContactDialog,
}) => (
  <ul className="flex items-center">
    {headers.map(({ label, ref }) => (
      <li
        key={label}
        className="cursor-pointer py-5 px-4 hover:text-gray-400"
        onClick={() => handleMenuClicked(ref)}
      >
        {label}
      </li>
    ))}

    <li className="ml-4">
      <Button onClick={openContactDialog}>Contact Me</Button>
    </li>

    <li
      className="ml-2 cursor-pointer py-5 px-4 hover:text-gray-400"
      onClick={() => setDarkMode(!darkMode)}
    >
      {darkMode ? <IoMoonSharp /> : <IoMoonOutline />}
    </li>
  </ul>
);

export default HeaderDesktop;
