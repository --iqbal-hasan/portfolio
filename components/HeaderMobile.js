import { useState } from "react";
import {
  IoMoonOutline,
  IoMoonSharp,
  IoMenuOutline,
  IoCloseOutline,
} from "react-icons/io5";

import { handleMenuClicked } from "@utils/methods";

import Button from "./Button";

const HeaderMobile = ({
  darkMode,
  setDarkMode,
  headers,
  openContactDialog,
}) => {
  // state
  const [openMenu, setOpenMenu] = useState(false);

  const openMenuHandler = () => {
    setOpenMenu(true);
  };

  const closeMenuHandler = () => {
    setOpenMenu(false);
  };

  return (
    <>
      <ul className="flex items-center">
        <li
          className="cursor-pointer py-5 px-3 hover:text-gray-400"
          onClick={() => setDarkMode(!darkMode)}
        >
          {darkMode ? <IoMoonSharp /> : <IoMoonOutline />}
        </li>

        <li
          className="cursor-pointer py-5 px-3 hover:text-gray-400"
          onClick={openMenuHandler}
        >
          <IoMenuOutline className="w-6 h-6" />
        </li>
      </ul>

      {openMenu && (
        <>
          <div
            className="fixed right-0 top-0 w-full h-screen z-40 bg-black bg-opacity-50"
            onClick={closeMenuHandler}
          />
          <div
            className="fixed right-0 top-0 w-full h-screen bg-white dark:bg-dark1 z-50 border-l border-gray-200 dark:border-gray-600 overflow-auto pb-16"
            style={{ maxWidth: "250px" }}
          >
            <div className="px-4 flex justify-between items-center border-b border-gray-200 dark:border-gray-600">
              <div className="w-full">iqbalhasan.web.id</div>

              <div
                onClick={closeMenuHandler}
                className="cursor-pointer hover:text-gray-400 px-3 py-5"
              >
                <IoCloseOutline className="w-6 h-6" />
              </div>
            </div>

            <ul className="py-3">
              {headers.map(({ label, ref }) => (
                <li
                  key={label}
                  className="cursor-pointer py-3 px-4 hover:text-gray-400"
                  onClick={() => handleMenuClicked(ref)}
                >
                  {label}
                </li>
              ))}

              <li className="ml-4 mt-3">
                <Button onClick={openContactDialog}>Contact Me</Button>
              </li>
            </ul>
          </div>
        </>
      )}
    </>
  );
};

export default HeaderMobile;
