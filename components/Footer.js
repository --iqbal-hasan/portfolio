import { useContext, useMemo } from "react";

import { MenuContext } from "@pages/_app";

import { handleMenuClicked } from "@utils/methods";

import { SOSMED_DATA } from "@data/sosmed";

const Footer = () => {
  // context
  const { aboutRef, expertiseRef, journeyRef, portfolioRef } =
    useContext(MenuContext);

  const footers = useMemo(
    () => [
      {
        label: "About Me",
        ref: aboutRef,
      },
      {
        label: "Expertise",
        ref: expertiseRef,
      },
      {
        label: "Journey",
        ref: journeyRef,
      },
      {
        label: "Portfolio",
        ref: portfolioRef,
      },
    ],
    []
  );

  return (
    <footer className="w-full border-t bg-gray-50 dark:bg-dark2 border-gray-200 dark:border-gray-600 px-4">
      <div className="max-w-7xl w-full mx-auto">
        <div className="flex flex-wrap py-8">
          <div className="w-full md:w-1/3 mb-6 md:mb-0">
            <h1 className="text-2xl font-bold text-center md:text-left">
              iqbalhasan.web.id
            </h1>
          </div>

          <div className="w-full md:w-1/3 mb-6 md:mb-0">
            <ul className="flex flex-wrap">
              {footers.map(({ label, ref }) => (
                <li
                  key={label}
                  className="w-full md:w-1/2 text-center md:text-left cursor-pointer hover:text-gray-400 px-2 mb-2"
                  onClick={() => handleMenuClicked(ref)}
                >
                  {label}
                </li>
              ))}
            </ul>
          </div>

          <div className="w-full md:w-1/3">
            <h1 className="text-2xl mb-5 font-bold text-center md:text-left">
              Social Media
            </h1>

            <ul className="flex flex-wrap justify-center md:justify-start">
              {SOSMED_DATA.map((item) => (
                <li key={item.id} className="mr-4">
                  <a
                    href={item.link}
                    target="_blank"
                    rel="noopener"
                    className="text-3xl"
                    style={item.style}
                  >
                    {item.icon}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className="py-4 border-t border-gray-200 dark:border-gray-600">
          <p className="text-center text-sm font-semibold">
            Made with Next JS by Iqbal Hasan
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
