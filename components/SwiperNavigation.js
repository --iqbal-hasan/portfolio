import { ChevronLeft, ChevronRight } from "react-iconly";

const buttonClassName =
  "z-10 absolute top-1/2 -translate-y-1/2 p-2 rounded-full bg-primary text-white shadow hover:bg-opacity-75";

const SwiperNavigation = ({ activityHover, prev, next }) => (
  <>
    <button
      className={`left-2 ${buttonClassName} ${prev}`}
      style={{
        visibility: activityHover ? "visible" : "hidden",
      }}
    >
      <ChevronLeft size="small" stroke="bold" />
    </button>

    <button
      className={`right-2 ${buttonClassName} ${next}`}
      style={{
        visibility: activityHover ? "visible" : "hidden",
      }}
    >
      <ChevronRight size="small" stroke="bold" />
    </button>
  </>
);

export default SwiperNavigation;
