import { useContext } from "react";
import Image from "next/image";
import { AiOutlinePhone } from "react-icons/ai";
import { BiMovie } from "react-icons/bi";
import {
  IoPersonOutline,
  IoMailOutline,
  IoMusicalNotesOutline,
  IoLaptopOutline,
  IoGameControllerOutline,
} from "react-icons/io5";

import useWindowSize from "@hooks/useWindowSize";

import { MenuContext } from "@pages/_app";

const ABOUT_DATA = [
  {
    label: "Iqbal Hasan",
    icon: <IoPersonOutline />,
  },
  {
    label: "082128424847",
    icon: <AiOutlinePhone />,
  },
  {
    label: "iqbalhasan387@gmail.com",
    icon: <IoMailOutline />,
  },
];

const INTEREST_DATA = [
  {
    label: "Music",
    icon: <IoMusicalNotesOutline />,
  },
  {
    label: "Movie",
    icon: <BiMovie />,
  },
  {
    label: "Programming",
    icon: <IoLaptopOutline />,
  },
  {
    label: "Game",
    icon: <IoGameControllerOutline />,
  },
];

const About = () => {
  // size
  const size = useWindowSize();

  // context
  const { aboutRef } = useContext(MenuContext);

  return (
    <section
      ref={aboutRef}
      className="max-w-7xl w-full py-32 mx-auto flex flex-wrap items-center justify-between"
    >
      <div className="w-full md:w-1/2 px-4 flex justify-center mb-10 md:mb-0">
        <Image
          src="/about.svg"
          alt="About"
          width={size.width >= 768 ? 450 : 300}
          height={size.width >= 768 ? 450 : 300}
        />
      </div>

      <div className="w-full md:w-1/2 flex flex-col items-center md:items-start space-y-4">
        <h1 className="text-5xl font-bold pb-4">About Me</h1>

        <p className="text-gray-500 dark:text-gray-300 text-center md:text-left">
          Have a passion in programming, especially on Frontend using React.
          Have good logic and able to develop themselves. Able to adapt to the
          environment and have a willingness to learn new knowledge.
        </p>

        <ul className="py-2 flex flex-col items-center md:items-start space-y-2">
          {ABOUT_DATA.map((item) => (
            <li
              key={item.label}
              className="flex items-center space-x-4 rounded-full py-2 px-6 border border-primary"
            >
              <h4 className="text-xl font-bold text-primary">{item.icon}</h4>

              <p className="text-sm italic text-gray-700 dark:text-gray-300">
                {item.label}
              </p>
            </li>
          ))}
        </ul>

        <h3 className="text-2xl font-bold pb-2">My Interest</h3>

        <ul className="flex flex-wrap justify-center md:justify-start space-y-2">
          {INTEREST_DATA.map((item) => (
            <li key={item.label} className="flex items-center space-x-3 pr-8">
              <h3 className="text-3xl text-primary">{item.icon}</h3>

              <p className="italic text-gray-500 dark:text-gray-300">
                {item.label}
              </p>
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default About;
