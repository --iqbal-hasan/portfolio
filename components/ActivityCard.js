"use client";

import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper/modules";
import Image from "next/image";

import SwiperNavigation from "./SwiperNavigation";
import Button from "./Button";

const prev = "activity-card-prev";
const next = "activity-card-next";

const ActivityCard = ({ images, title, url, technologies, categories }) => {
  // state
  const [activityHover, setActivityHover] = useState(false);

  return (
    <div
      className="relative w-full h-full lg:hover:scale-105 duration-300 rounded-lg overflow-hidden border border-divider-border flex flex-col"
      onMouseEnter={() => setActivityHover(true)}
      onMouseLeave={() => setActivityHover(false)}
    >
      <div className="w-full">
        <Swiper
          pagination={{
            clickable: true,
            dynamicBullets: true,
          }}
          navigation={{
            enabled: true,
            prevEl: `.${prev}`,
            nextEl: `.${next}`,
            disabledClass: "hidden",
          }}
          modules={[Navigation, Pagination]}
          className="mySwiper"
        >
          {images.map((image, index) => (
            <SwiperSlide key={`${title}-${index}`}>
              <div className="relative w-full pb-[62.5%]">
                <Image
                  src={image}
                  alt={title}
                  width={520}
                  height={300}
                  sizes="320px"
                  objectFit="cover"
                  className="rounded-t-lg"
                />
              </div>
            </SwiperSlide>
          ))}

          <SwiperNavigation
            activityHover={activityHover}
            prev={prev}
            next={next}
          />
        </Swiper>
      </div>

      <div className="flex-1 p-4 flex flex-col justify-between gap-4">
        <div className="w-full flex flex-col gap-2">
          <div className="w-full flex items-center gap-2 overflow-hidden">
            {categories.map((category, index) => (
              <div
                key={`category-${title}-${index}}`}
                className="py-1 px-2 rounded-md bg-primary bg-opacity-10 border border-primary"
              >
                <p className="text-xs whitespace-nowrap">{category}</p>
              </div>
            ))}
          </div>

          <h2 className="text-lg font-medium multiline-ellipsis">{title}</h2>

          <div className="flex flex-wrap items-center justify-start gap-2">
            {technologies.map(({ label, icon, style }) => (
              <div key={label} className="rounded-lg bg-gray-200 p-2">
                <h1 className="text-base" style={style}>
                  {icon}
                </h1>
              </div>
            ))}
          </div>
        </div>

        {url ? (
          <div className="w-full">
            <a href={url} target="_blank" rel="noopener noreferrer">
              <button className="w-full py-2 bg-primary text-white rounded-md hover:opacity-75 text-sm">
                View Website
              </button>
            </a>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ActivityCard;
