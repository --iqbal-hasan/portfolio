import React, { useContext, useState, useEffect } from "react";

import { MenuContext } from "@pages/_app";

import { PORTFOLIO_DATA } from "@data/index";

import ActivityCard from "./ActivityCard";

const Portfolio = () => {
  // context
  const { portfolioRef } = useContext(MenuContext);

  // state
  const [portfolios, setPortfolios] = useState([]);

  // effect
  useEffect(() => {
    setPortfolios([...PORTFOLIO_DATA]);
  }, []);

  return (
    <section ref={portfolioRef} className="max-w-7xl w-full py-32 mx-auto">
      <div className="mb-14 text-center">
        <h1 className="text-5xl font-bold mb-5">My Portfolio</h1>

        <p className="text-gray-500 dark:text-gray-290 max-w-md w-full mx-auto">
          Explore my Projects when I work as Frontend Developer.
        </p>
      </div>

      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-3 sm:gap-4">
        {Boolean(portfolios) &&
          portfolios.length > 0 &&
          portfolios.map((item, index) => (
            <div
              key={`portfolio-${index}`}
              className="justify-self-center w-full min-w-0 max-w-xs md:max-w-none"
            >
              <ActivityCard {...item} />
            </div>
          ))}
      </div>
    </section>
  );
};

export default Portfolio;
