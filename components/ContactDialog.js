import { forwardRef, useState, useImperativeHandle } from "react";

const { AiOutlinePhone } = require("react-icons/ai");
const { IoCloseOutline, IoMailOutline } = require("react-icons/io5");

const ContactDialog = forwardRef((_, ref) => {
  // state
  const [open, setOpen] = useState(false);

  useImperativeHandle(ref, () => ({
    handleClickOpen: () => {
      setOpen(true);
    },
  }));

  const handleClose = (e) => {
    e.stopPropagation();

    setOpen(false);
  };

  return (
    <>
      {open && (
        <div className="fixed left-0 top-0 bg-black bg-opacity-50 w-full h-screen z-50">
          <div
            className="w-full h-screen flex justify-center items-center"
            onClick={handleClose}
          >
            <div
              className="bg-white dark:bg-dark1 rounded"
              onClick={(e) => e.stopPropagation()}
            >
              <div className="py-3 px-6 flex justify-between items-center border-b border-gray-200 dark:border-gray-600">
                <div>Please Contact Me Via</div>
                <div>
                  <IoCloseOutline
                    className="h-7 w-7 cursor-pointer"
                    onClick={handleClose}
                  />
                </div>
              </div>

              <div className="py-3 px-6">
                <ul className="py-2 space-y-2">
                  <li className="flex items-center space-x-4">
                    <h4 className="flex space-x-2 text-xl font-bold w-16">
                      <AiOutlinePhone />{" "}
                      <span className="text-sm font-normal">Telp</span>
                    </h4>

                    <p className="text-sm italic text-gray-700 dark:text-gray-300">
                      : 082128424847
                    </p>
                  </li>

                  <li className="flex items-center space-x-4">
                    <h4 className="flex space-x-2 text-xl font-bold w-16">
                      <IoMailOutline />{" "}
                      <span className="text-sm font-normal">Email</span>
                    </h4>

                    <p className="text-sm italic text-gray-700 dark:text-gray-300">
                      : iqbalhasan387@gmail.com
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
});

export default ContactDialog;
