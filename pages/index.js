import Head from "next/head";

import Banner from "@components/Banner";
import About from "@components/About";
import Expertise from "@components/Expertise";
import Journey from "@components/Journey";
import Portfolio from "@components/Portfolio";

const Home = () => (
  <div>
    <Head>
      <title>Iqbal Hasan</title>

      <meta name="title" content="Portfolio Iqbal Hasan" />
      <meta name="subject" content="Portfolio" />
      <meta
        name="description"
        content="Portfolio Iqbal Hasan as Frontend Developer"
      />
      <meta name="format" content="text/html" />
      <meta name="publisher" content="Iqbal Hasan" />
      <meta name="language" content="en" />
      <meta httpEquiv="Content-Type" content="text/html;charset=UTF-8" />

      <link rel="icon" href="/favicon.ico" />
    </Head>

    <main className="px-4">
      <Banner />
      <About />
      <Expertise />
      <Journey />
      <Portfolio />
    </main>
  </div>
);

export default Home;
