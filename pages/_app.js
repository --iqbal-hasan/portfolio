import { createContext, useMemo, useRef, useState, useEffect } from "react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

import "@styles/globals.css";

import Header from "@components/Header";
import Footer from "@components/Footer";

export const MenuContext = createContext({});

const MyApp = ({ Component, pageProps }) => {
  // ref
  const homeRef = useRef(null);
  const aboutRef = useRef(null);
  const expertiseRef = useRef(null);
  const journeyRef = useRef(null);
  const portfolioRef = useRef(null);
  const testimonialRef = useRef(null);

  // state
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    const init_dark_mode =
      typeof window !== "undefined"
        ? window.localStorage.getItem("darkMode")
        : "false";

    setDarkMode(init_dark_mode === "true");
  }, []);

  const handleDarkMode = (isDarkMode) => {
    setDarkMode(isDarkMode);
    localStorage.setItem("darkMode", isDarkMode);
  };

  const menuRef = useMemo(
    () => ({
      homeRef,
      aboutRef,
      expertiseRef,
      journeyRef,
      portfolioRef,
      testimonialRef,
    }),
    [homeRef, aboutRef, expertiseRef, journeyRef, portfolioRef, testimonialRef]
  );

  return (
    <MenuContext.Provider value={menuRef}>
      <div className={darkMode ? "dark" : ""}>
        <div className="bg-white dark:bg-dark1 text-textLight dark:text-white">
          <Header darkMode={darkMode} setDarkMode={handleDarkMode} />

          <Component {...pageProps} />

          <Footer />
        </div>
      </div>
    </MenuContext.Provider>
  );
};

export default MyApp;
