module.exports = {
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  async rewrites() {
    return [
      {
        source: "/robots.txt",
        destination: "/api/robots",
      },
    ];
  },
  images: {
    imageSizes: [
      16, 32, 40, 48, 64, 96, 128, 138, 144, 164, 192, 208, 256, 320, 360, 384,
      400, 520,
    ],
  },
};
