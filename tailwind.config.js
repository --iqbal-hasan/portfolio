module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        dark1: "#15202b",
        dark2: "#1B2836",
        primary: "#FF5B00",
        secondary: "#FF5B00",
        textLight: "rgb(18, 18, 18)",
        textDark: "#FFF",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
