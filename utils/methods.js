export const handleMenuClicked = (menuRef) => {
  menuRef.current.scrollIntoView({ behavior: "smooth" });
};
